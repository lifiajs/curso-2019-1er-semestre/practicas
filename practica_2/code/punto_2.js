// A

/**
 *
 * @param {Array} arr Array of int
 * @returns {Array}
 */
function squares(arr) {
  if (typeof(arr) === 'undefined') {
    return [];
  }

  return Array.from(arr || []).map(function(x) {
    return x * 2;
  });
}

// ----------------------------------------------------------------------------------------
// B

/**
 * 
 * @param {Array} arr Array of Objects
 * @returns {Array}
 */
function getPeople(arr) {
  if (typeof(arr) === 'undefined') {
    return [];
  }

  return Array.from(arr || []).filter(function(elem) {
    return elem.name && elem.lastName;
  })
}

// ----------------------------------------------------------------------------------------
// C

/**
 * 
 * @param {Array} arr Array of pairs
 * @returns {Array}
 */
function objectFromEntries(arr) {
  if (typeof(arr) === 'undefined') {
    return [];
  }

  return Array.from(arr || []).reduce(function(obj, entries) {
    obj[entries[0]] = entries[1];
    return obj;
  }, {});
}

// ----------------------------------------------------------------------------------------
// D
var tags = [ 'h1', 'h2', 'h3', 'h4', 'h5', 'p' ]


/**
 * 
 * @param {String} tag one of h1, h2, h3, h4, h5 or p
 * @param {String} str Text to append as tag's child
 * @returns {String} HTML valid
 * @see {@link https://htmlhint.io/}
 * @see {@link tags}
 */
function toHtml(tag, str) {
  var validTag = false;
  for (i = 0; i < tags.length; i++) {
    if (tags[i] === tag) {
      validTag = true;
    }
  }

  if (!validTag) {
    return '';
  }

  if (str === undefined) {
    return '<' + tag + '></' + tag + '>';
  }

  return '<' + tag + '>' + str + '</' + tag + '>';
}

// ----------------------------------------------------------------------------------------
// E

/**
 * Convert an array to Yml notation
 * @param {Array} array Array to convert to Yml notation
 * @param {Number} level Tab level
 * @returns {String} Yml valid - Test with
 * @see {@link http://www.yamllint.com/}
 */
function toYmlArray(array, level) {
  if (array === undefined || !Array.isArray(array)) {
    return '';
  }
  var tab = '';
  
  if (typeof(level) === 'undefined') {
    level = 0;
  } else {
    for (i = 0; i < level; i++) {
      tab += '  ';
    }
  }

  return array.reduce(function(str, elem) {
    return str += tab + '- ' + elem + '\n';
  }, '');
}

/**
 * Convert any JSON valid to Yml notation
 * @param {Object} data Object to convert to Yml notation
 * @param {Number} level Tab level
 * @see {@link http://www.yamllint.com/ }
 */
function toYml(data, level) {
  var yml = '';
  var tab = '';

  if (typeof(level) === 'undefined') {
    level = 0;
  } else {
    for (i = 0; i < level; i++) {
      tab += '  ';
    }
  }

  Object.keys(data).forEach(function(elem) {
    if (typeof(data[elem]) === 'object' && !Array.isArray(data[elem])) {
      yml += elem + ': \n';
      yml += toYml(data[elem], level+1);
    } else {
      if (Array.isArray(data[elem])) {
        yml += tab + elem + ':\n' + toYmlArray(data[elem], level+1);
      } else {
        yml += tab + elem + ': ' + data[elem];
      }
    }
    yml += '\n'
  })

  return yml;
}