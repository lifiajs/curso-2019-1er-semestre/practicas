/**
 * Pasa a mayúscula la primer letra del string
 * Ej: "people group".capitalize() === "People group"
 * Ej: capitalize("people group") === "People group"
 * @returns {String}
 */
function capitalize() {}

/**
 * Convierte el string a snakecase
 * Los espacios se reemplazan por _
 * Ej: "person to yml".snakeize() === "person_to_yml"
 * Ej: snakeize("person to yml") === "person_to_yml"
 * @returns {String} 
 */
function snakeize() {}

/**
 * Convierte el string a camelcase
 * Los espacios se reemplazan por la siguiente letra en mayúscula
 * Ej: "person to yml".camelize() === "personToYml"
 * Ej: camelize("person to yml") === "personToYml"
 * @returns {String} 
 */
function camelize() {}

/**
 * Convierte el string a un formato de clase válido
 * Formato camelcase capitalizado
 * Ej: "people group".clasify() === "PeopleGroup"
 * Ej: clasify("people group") === "PeopleGroup"
 * @returns {String} 
 */
function classify() {}