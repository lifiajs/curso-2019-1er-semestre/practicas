# Práctica 3

> EcmaScript 6, React JS, Componentes, Ciclo de Vida, Propiedades, Estado interno

* * *

### Materiales

-  [Ejemplos interactivos](https://codesandbox.io/s/github/LIFIAJS/curso-2019/tree/develop/src/examples/frontend/react)

### Entornos

-  [CodePen](https://reactjs.org/redirect-to-codepen/hello-world)
-  [CodeSandbox](https://codesandbox.io/s/new)
-  [Glitch](https://glitch.com/edit/#!/remix/starter-react-template)

* * *

## Ejercicio 1

En la página de [Ejemplos interactivos](https://codesandbox.io/s/github/LIFIAJS/curso-2019/tree/develop/src/examples/frontend/react) se encuentran cada uno de los ejemplos utilizados en la teoría. Analizar los archivos y observar como renderizar cada uno de ellos.

<div style="width: 100%; text-align: right; font-style: italic; font-size: 12px;">
Se recomienda consultar cualquier duda relacionada al renderizado y distribución de directorio con los docentes del curso.

Se recomienda hacer un fork del repositorio en codesandbox para la realización del resto de los ejercicios.
</div>

## Ejercicio 2

Analizar el ciclo de vida y ejecución de la componente en el ejemplo `stateful-component`. Pensar cómo afectaría al ciclo de vida si el atributo `interval` perteneciese al estado interno de la componente. Ver que pasaría si se ejecutara la función `this.setState` en el `componentDidUpdate`.

<div style="width: 100%; text-align: right; font-style: italic; font-size: 12px;">
Se recomienda utilizar funcionalidades tales como <em style="color: orange">console.log</em> o <em style="color: orange">console.dir</em> para facilitar el análisis.
</div>

## Ejercicio 3

Modificar el ejemplo `people` para que a la componente `People` le llegue en sus _propiedades_ una collección de elementos renderizables, _Componentes instanciadas_, permitiendo visualizar los mismos datos que el ejemplo actual.

Para pensar, entre el ejemplo original y el cambio realizado, ¿cuál implementación parece más adecuada? ¿En que casos utilizaría cada una?

<div style="width: 100%; text-align: right; font-style: italic; font-size: 12px;">
Discutir con algún docente respecto de esta última pregunta.
</div>

## Ejercicio 4

> Ver archivo `src/todo-app.js`.

1.  Analizar el funcionamiento e implementación del `todo-app`.

2.  Analizar la importancia del siguiente bloque de código en el contructor de la clase `TodoApp`.

```jsx
this.handleChange = this.handleChange.bind(this)
this.handleSubmit = this.handleSubmit.bind(this)
```

## Ejercicio 5

En el archivo `src/todo-app.js` se encuentra el siguiente bloque de código.

```jsx
ReactDOM.render(
  <TodoApp />,
  document.getElementById('root')
)
```

Se desea cambiar el ejemplo de forma tal que la compenente pueda ser importada y renderizada en el archivo `src/index.js`.

## Ejercicio 6

> Extender el funcionamiento de la componente `TodoApp`. 

Se desea obtener en principio una funcionalidad similar a la implementación realizada en la _Práctica 1_, pero esta vez programando con ReactJS.

Las funcionalidades a agregar son:

-  Finalizar una tarea, moviendo la misma a una lista nueva de _tareas finalizadas_.
-  Limpiar todos los listados.

Para esto se deben dsicriminar las tareas entre _tareas a realizar_ y _tareas finalizadas_.

_No es necesario persistir las tareas en ningun localStorage_.

## Ejercicio 7

Extender la implementación anterior agregando la siguiente funcionalidad funcionalidad:

-  Deshacer el borrado de una tarea.
-  Mostrar fecha de finalización de una tarea.

## Ejercicio 8

Extender la implementación anterior de forma tal que las tareas puedan ser persistidas.

_Analizar posibles implementaciones con los docentes_.
