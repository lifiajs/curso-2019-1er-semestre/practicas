# Práctica 4

> Webpack, Babel, ESLint, React Redux + Webpack Starter, Redux

* * *

### Materiales

-  [React Redux + Webpack Starter](https://github.com/ulises-jeremias/react-redux-webpack-starter)

* * *

## Ejercicio 1

Crear una aplicación `my_app` utilizando [React Redux + Webpack Starter](https://github.com/ulises-jeremias/react-redux-webpack-starter).

## Ejercicio 2

Identificar los archivos de configuración de Webpack, ESLint y Babel. Analizar que scripts del `package.json` utilizan estas configuraciones.

## Ejercicio 3

Migrar el `TodoApp` realizado en la practica anterior al proyecto creado, `my_app`. Debe respetarse el patron de directorios planteados para `Container-Presentational`.

## Ejercicio 4

> ESLint

Copiar el siguiente código en el archivo `src/components/Timer.jsx` y renderizarlo en algun container, por ejemplo, `HomeContainer`. Realizar los cambios necesarios para que el mismo funcione correctamente en el proyecto generado.

```jsx
import React from 'react'
import ReactDOM from 'react-dom'
class Timer extends React.Component {
state = {seconds: 0}
tick = () => {this.setState(state => ({ seconds: state.seconds + 1 }))}
componentDidMount() {this.interval = setInterval(this.tick, 1000)}
componentWillUnmount() {clearInterval(this.interval)}
render() {
return (
<div>Seconds: {this.state.seconds}</div>
)
}
}
export default Timer
```

## Ejercicio 5

Realizar la misma tarea que en el ejercicio anterior, para la componente `LoginForm`.

```jsx
import React from 'react'
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'

const LoginForm = () => (
<div className='login-form'>
{/*
Heads up! The styles below are necessary for the correct render of this example.You can do same with CSS, the main idea is that all the elements up to the `Grid`below must have a height of 100%.
*/}
<style>{`body > div,body > div > div,body > div > div > div.login-form {height: 100%;}`}
</style>
<Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'><Grid.Column style={{ maxWidth: 450 }}><Header as='h2' color='teal' textAlign='center'><Image src='/logo.png' /> Log-in to your account</Header><Form size='large'><Segment stacked><Form.Input fluid icon='user' iconPosition='left' placeholder='E-mail address' /><Form.Input fluidicon='lock' iconPosition='left' placeholder='Password' type='password' />
<Button color='teal' fluid size='large'>Login</Button></Segment></Form><Message>New to us? <a href='#'>Sign Up</a></Message></Grid.Column>
</Grid>
</div>
)

export default LoginForm
```

## Ejercicio 6

Analizar las reglas presentes en el archivo `.eslintrc.js`. Analizar donde y como se ejecuta linter para detectar los errores en cada cambio de código.

# Redux

## Ejercicio 7

Analizar la configuración de redux en el proyecto generado. Cambiar la implementación del TodoApp migrado para que el mismo utilice `redux`.
Para este ejercicio es necesario persistir los datos en el `localStorage`. Será necesario evaluar cuidadosamente donde y como acceder a los datos del mismo.

Dada la ejecución de una acción de redux, se deberán conocer tres posibles estados.

-  `request`, informa que se inicia el proceso de ejecución de una acción.
-  `success`, informa que la acción se ejecutó en forma exitosa.
-  `failure`, informa que existe un error en la ejecución de la acción.

Por ejemplo, si se desea realizar una solicitud de datos al localStorage, un posible nombre para el tipo puede ser `tasks-get`. En ese caso, deberá contarse con los siguientes estados,

- `tasks-get-request`
- `tasks-get-success`
- `tasks-get-failure`

Luego, las constantes podrían quedar de la siguiente forma:

`src/constants/tasks.constants.js`

```javascript
export const TASKS_GET_REQUEST = 'tasks-get-request'
export const TASKS_GET_SUCCESS = 'tasks-get-success'
export const TASKS_GET_FAILURE = 'tasks-get-failure'
```
